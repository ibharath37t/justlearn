package com.example.hp.justlearn.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hp.justlearn.beans.ListBean;
import com.example.hp.letsjustlearn.R;

import java.util.List;

/**
 * Created by hp on 17-Mar-18.
 */

public class LorSListAdapter extends RecyclerView.Adapter<LorSListAdapter.MyViewHolder> {

    private List<ListBean> listBeans;
    private static LorSListAdapter.OnItemClickListener mListener;

    // Define the mListener interface
    public interface OnItemClickListener {

        void onItemClick(int position);

    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(LorSListAdapter.OnItemClickListener listener) {

        this.mListener = listener;

    }

    public LorSListAdapter(List<ListBean> list) {

        this.listBeans = list;
        setHasStableIds(true);
        this.notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView numtext, cattext, lostext, parttext;

        public MyViewHolder(View view) {
            super(view);

            numtext = view.findViewById(R.id.numtext);
            cattext = view.findViewById(R.id.cattext);
            lostext = view.findViewById(R.id.lostext);
            parttext = view.findViewById(R.id.parttext);


            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mListener != null) mListener.onItemClick(getAdapterPosition());

                }

            });

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ListBean item = listBeans.get(position);
        holder.numtext.setText("" + (position + 1));
        holder.cattext.setText(item.getCategory());
        holder.lostext.setText(item.getWish());
        holder.parttext.setText(item.getParticulars());

    }


    @Override
    public int getItemCount() {
        return listBeans.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
