package com.example.hp.justlearn.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.hp.justlearn.beans.ListBean;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "justlearn_db";


    // Create table SQL query
    public static final String TABLE_LorS = "learnshareTable";
    public static final String T_ID = "id";
    public static final String COLUMN_DISC = "discipline";
    public static final String COLUMN_SUB = "subject";
    public static final String COLUMN_WISH = "wish";

    public static final String CREATE_LorS =
            "CREATE TABLE " + TABLE_LorS + "("
                    + T_ID + " INTEGER PRIMARY KEY, "
                    + COLUMN_DISC + " TEXT, "
                    + COLUMN_SUB + " TEXT, "
                    + COLUMN_WISH + " TEXT "
                    + ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(CREATE_LorS);

    }


    public void deleteFromAllTables() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_LorS);
        db.close();

    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LorS);
        // Create tables again
        onCreate(db);

    }


    public long insertTableWish(String disc, String sub, String wish) {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_DISC, disc);
        values.put(COLUMN_SUB, sub);
        values.put(COLUMN_WISH, wish);

        // insert row
        long id = db.insert(TABLE_LorS, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return id;

    }


    public ArrayList<ListBean> getWishes() {
        ArrayList<ListBean> listBeans = new ArrayList<ListBean>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LorS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                ListBean bean = new ListBean();
                bean.setCategory(cursor.getString(cursor.getColumnIndex(COLUMN_DISC)));
                bean.setParticulars(cursor.getString(cursor.getColumnIndex(COLUMN_SUB)));
                bean.setWish(cursor.getString(cursor.getColumnIndex(COLUMN_WISH)));
                listBeans.add(bean);
            } while (cursor.moveToNext());

        }

        // close db connection
        db.close();
        // return notes list
        return listBeans;

    }


    public void deleteAllWishes() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_LorS);
        db.close();

    }


}
