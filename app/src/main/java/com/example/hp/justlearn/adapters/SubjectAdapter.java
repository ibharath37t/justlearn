package com.example.hp.justlearn.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.justlearn.beans.DisciplineBean;
import com.example.hp.justlearn.beans.SubjectBean;
import com.example.hp.letsjustlearn.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by hp on 17-Mar-18.
 */

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyViewHolder> {

    private List<SubjectBean> subjectBeans;
    private static SubjectAdapter.OnItemClickListener mListener;

    // Define the mListener interface
    public interface OnItemClickListener {

        void onItemClick(int position);

    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(SubjectAdapter.OnItemClickListener listener) {

        this.mListener = listener;

    }

    public SubjectAdapter(List<SubjectBean> list) {

        this.subjectBeans = list;
        setHasStableIds(true);
        this.notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView discimg;
        public TextView disctext;

        public MyViewHolder(View view) {
            super(view);

            discimg = view.findViewById(R.id.sub_image);
            disctext = view.findViewById(R.id.sub_text);


            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mListener != null) mListener.onItemClick(getAdapterPosition());

                    if (itemView.getAlpha() == 0.5) {

                        itemView.setAlpha((float) 1);

                    } else {

                        itemView.setAlpha((float) 0.5);

                    }

                }

            });

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_subject, parent, false);

        return new MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        String url = subjectBeans.get(position).getImagurl();
        if (url.equals("") || url.isEmpty()) {

        } else {

            Picasso.get()
                    .load(url)
                    .error(R.drawable.erroricon)
                    .into(holder.discimg);

        }
        holder.disctext.setText(subjectBeans.get(position).getSub());

    }


    @Override
    public int getItemCount() {
        return subjectBeans.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
