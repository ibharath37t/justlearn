package com.example.hp.justlearn.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.justlearn.utils.ServiceHandler;
import com.example.hp.letsjustlearn.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {


    Button regBtn;
    EditText name, mob, email, pwd, cnfpwd, add;
    ImageView logImg, backImg;
    String names = "", mobs = "", emails = "", pwds = "", cnfpwds = "";
    public static String lat = "", lon = "", adds = "";
    TextView regText;
    String reg_response = "", reg_url = "";
    ProgressDialog progressDialog;
    ImageView mapBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        regBtn = findViewById(R.id.regBtn);
        backImg = findViewById(R.id.logout);
        backImg.setVisibility(View.INVISIBLE);
        logImg = findViewById(R.id.back);
        logImg.setVisibility(View.INVISIBLE);
        name = findViewById(R.id.nametext);
        mob = findViewById(R.id.mobtext);
        email = findViewById(R.id.emailtext);
        pwd = findViewById(R.id.pwdtext);
        cnfpwd = findViewById(R.id.cnfpwdtext);
        regText = findViewById(R.id.regText);
        add = findViewById(R.id.addresstext);
        mapBtn = findViewById(R.id.mapbutton);


        regText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //finish();
                Intent intent = new Intent(RegisterActivity.this, MapsActivity.class);
                startActivityForResult(intent, 1);

            }
        });


        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reg_url = getString(R.string.register);

                names = name.getText().toString();
                mobs = mob.getText().toString();
                emails = email.getText().toString();
                pwds = pwd.getText().toString();
                cnfpwds = cnfpwd.getText().toString();
                adds = add.getText().toString();

                String mobilePattern = "[0-9]{10}";
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (names.isEmpty() || names.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter name", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (mobs.isEmpty() || mobs.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter mobile number", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (mobs.length() != 10 || !mobs.matches(mobilePattern)) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter valid mobile number", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (emails.isEmpty() || emails.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter email id", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (!emails.matches(emailPattern)) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter valid email id", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (pwds.isEmpty() || pwds.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter password", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (cnfpwds.isEmpty() || cnfpwds.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please confirm password", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (!pwds.equals(cnfpwds)) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Password doesn't match", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (adds.isEmpty() || adds.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter address", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else {

                    ConnectivityManager manager =
                            (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

                    if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                            manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                            manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                            manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

                        //new RegistrationValidation().execute();

                        finish();
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent);

                    } else {

                        Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                                "No Internet, please try again!", Snackbar.LENGTH_SHORT);
                        View sbView = mySnackbar.getView();
                        sbView.setBackgroundColor(Color.GRAY);
                        mySnackbar.show();

                    }

                }

            }
        });

    }


    public class RegistrationValidation extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();


        @Override
        protected void onPreExecute() {

            progressDialog = new ProgressDialog(RegisterActivity.this);
            progressDialog.setMessage("Registering please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();

            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> valuePairs = new ArrayList<>(7);
            valuePairs.add(new BasicNameValuePair("name", names));
            valuePairs.add(new BasicNameValuePair("mobile", mobs));
            valuePairs.add(new BasicNameValuePair("email", emails));
            valuePairs.add(new BasicNameValuePair("password", pwds));
            valuePairs.add(new BasicNameValuePair("latitude", lat));
            valuePairs.add(new BasicNameValuePair("longitude", lon));
            valuePairs.add(new BasicNameValuePair("address", adds));


            try {

                reg_response = serviceHandler.makeServiceCall(reg_url, serviceHandler.POST, valuePairs);

            } catch (Exception e) {
                e.printStackTrace();
                reg_response = "Fail";
            }

            JSONObject jsonObject = null;
            try {

                jsonObject = new JSONObject(reg_response);

                if (jsonObject.length() != 0) {

                    //userid = jsonObject.getJSONArray("detail").getJSONObject(0).getString("userid");

                } else {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please try again!", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.dismiss();
            finish();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);

            super.onPostExecute(aVoid);
        }


    }

}
