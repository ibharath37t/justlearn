package com.example.hp.justlearn.beans;

public class DisciplineBean {

    private String did, disc, imagurl;

    public DisciplineBean() {

    }

    public DisciplineBean(String did, String disc, String imagurl) {
        this.did = did;
        this.disc = disc;
        this.imagurl = imagurl;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getImagurl() {
        return imagurl;
    }

    public void setImagurl(String imagurl) {
        this.imagurl = imagurl;
    }

}
