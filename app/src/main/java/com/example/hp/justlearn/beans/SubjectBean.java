package com.example.hp.justlearn.beans;

public class SubjectBean {

    private String sid, sub, imagurl;

    public SubjectBean() {

    }

    public SubjectBean(String sid, String sub, String imagurl) {
        this.sid = sid;
        this.sub = sub;
        this.imagurl = imagurl;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getImagurl() {
        return imagurl;
    }

    public void setImagurl(String imagurl) {
        this.imagurl = imagurl;
    }

}
