package com.example.hp.justlearn.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.justlearn.beans.DisciplineBean;
import com.example.hp.letsjustlearn.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by hp on 17-Mar-18.
 */

public class DisciplineAdapter extends RecyclerView.Adapter<DisciplineAdapter.MyViewHolder> {

    private List<DisciplineBean> disciplineBeans;
    private static DisciplineAdapter.OnItemClickListener mListener;

    // Define the mListener interface
    public interface OnItemClickListener {

        void onItemClick(int position);

    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(DisciplineAdapter.OnItemClickListener listener) {

        this.mListener = listener;

    }

    public DisciplineAdapter(List<DisciplineBean> list) {

        this.disciplineBeans = list;
        setHasStableIds(true);
        this.notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView discimg;
        public TextView disctext;


        public MyViewHolder(final View view) {
            super(view);

            discimg = view.findViewById(R.id.disc_image);
            disctext = view.findViewById(R.id.disc_text);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mListener != null)
                        mListener.onItemClick(getAdapterPosition());

                }

            });

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_discipline, parent, false);

        return new MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        String url = disciplineBeans.get(position).getImagurl();
        if (url.equals("") || url.isEmpty()) {

        } else {

            Picasso.get()
                    .load(url)
                    .error(R.drawable.erroricon)
                    .into(holder.discimg);

        }
        holder.disctext.setText(disciplineBeans.get(position).getDisc());

    }


    @Override
    public int getItemCount() {
        return disciplineBeans.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
