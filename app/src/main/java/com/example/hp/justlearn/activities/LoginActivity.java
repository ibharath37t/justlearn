package com.example.hp.justlearn.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.hp.justlearn.utils.ServiceHandler;
import com.example.hp.justlearn.utils.SessionManager;
import com.example.hp.letsjustlearn.R;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {


    EditText nametext, mobtext;
    Button getStartBtn;
    ImageView backImg, logImg;
    String name = "", mobile = "", userid = "";
    String login_response = "", login_url = "";
    ProgressDialog progressDialog;
    SessionManager sessionManager;
    TextView regText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        nametext = findViewById(R.id.nametext);
        mobtext = findViewById(R.id.mobiletext);
        getStartBtn = findViewById(R.id.getStartBtn);
        backImg = findViewById(R.id.logout);
        backImg.setVisibility(View.INVISIBLE);
        logImg = findViewById(R.id.back);
        logImg.setVisibility(View.INVISIBLE);
        sessionManager = new SessionManager(getApplicationContext());
        regText = findViewById(R.id.regText);


        regText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);

            }
        });


        getStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login_url = getString(R.string.login);

                name = nametext.getText().toString();
                mobile = mobtext.getText().toString();

                if (name.isEmpty() || name.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter name", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else if (mobile.isEmpty() || mobile.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter password", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                } else {

                    ConnectivityManager manager =
                            (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

                    if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                            manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                            manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                            manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

                        //new LoginValidation().execute();

                        finish();
                        Intent intent = new Intent(getApplicationContext(), LearnShareListActivity.class);
                        startActivity(intent);

                    } else {

                        Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                                "No Internet, please try again!", Snackbar.LENGTH_SHORT);
                        View sbView = mySnackbar.getView();
                        sbView.setBackgroundColor(Color.GRAY);
                        mySnackbar.show();

                    }

                }


            }
        });

    }


    public class LoginValidation extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();


        @Override
        protected void onPreExecute() {

            userid = "";
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Verifying please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();

            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> valuePairs = new ArrayList<>(2);
            valuePairs.add(new BasicNameValuePair("username", name));
            valuePairs.add(new BasicNameValuePair("password", mobile));


            try {

                login_response = serviceHandler.makeServiceCall(login_url, serviceHandler.POST, valuePairs);

            } catch (Exception e) {
                e.printStackTrace();
                login_response = "Fail";
            }

            JSONObject jsonObject = null;
            try {

                jsonObject = new JSONObject(login_response);

                if (jsonObject.length() != 0) {

                    userid = jsonObject.getJSONArray("detail").getJSONObject(0).getString("userid");

                } else {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Check Username or Password", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.GRAY);
                    mySnackbar.show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            if (userid.equals("")) {

                progressDialog.dismiss();
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                        "Please try again!", Snackbar.LENGTH_SHORT);
                View sbView = mySnackbar.getView();
                sbView.setBackgroundColor(Color.MAGENTA);
                mySnackbar.show();

            } else {

                progressDialog.dismiss();
                sessionManager.createLoginSession(userid, name, mobile);
                finish();
                Intent intent = new Intent(getApplicationContext(), LearnShareListActivity.class);
                startActivity(intent);

            }

            super.onPostExecute(aVoid);
        }


    }


}
