package com.example.hp.justlearn.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;
import com.example.hp.justlearn.adapters.DisciplineAdapter;
import com.example.hp.justlearn.adapters.LorSListAdapter;
import com.example.hp.justlearn.adapters.SubjectAdapter;
import com.example.hp.justlearn.beans.DisciplineBean;
import com.example.hp.justlearn.beans.ListBean;
import com.example.hp.justlearn.beans.SubjectBean;
import com.example.hp.justlearn.utils.DatabaseHelper;
import com.example.hp.justlearn.utils.ServiceHandler;
import com.example.hp.justlearn.utils.SessionManager;
import com.example.hp.letsjustlearn.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import belka.us.androidtoggleswitch.widgets.BaseToggleSwitch;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;

public class LearnShareListActivity extends AppCompatActivity {


    ImageView logoutImage, backImage, addImg;
    AlertDialog.Builder builder;
    SessionManager sessionManager;
    Button subBtn;
    RecyclerView recyclerView;
    LorSListAdapter lorSListAdapter;
    ArrayList<ListBean> listBeans = new ArrayList<ListBean>();
    DatabaseHelper helper;
    PopupWindow popupWindow;
    LinearLayout linearLayout1;
    RecyclerView discGrid, subGrid;
    ArrayList<DisciplineBean> disciplineBeans = new ArrayList<DisciplineBean>();
    DisciplineAdapter adapter;
    String disc_url = "", disc_resp = "";
    String disc = "", discid = "", discimgurl = "";
    ToggleSwitch toggleSwitch;
    ArrayList<SubjectBean> subjectBeans = new ArrayList<SubjectBean>();
    SubjectAdapter adapter2;
    String sub_url = "", sub_resp = "";
    String subid = "", sub = "", subimgurl = "";
    Button sBtn, cBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_share_list);


        logoutImage = findViewById(R.id.logout);
        backImage = findViewById(R.id.back);
        backImage.setVisibility(View.INVISIBLE);
        sessionManager = new SessionManager(getApplicationContext());
        addImg = findViewById(R.id.addBtn);
        subBtn = findViewById(R.id.submitBtn);
        recyclerView = findViewById(R.id.recycler_view);
        linearLayout1 = findViewById(R.id.linearLayout);
        helper = new DatabaseHelper(getApplicationContext());


        LayoutInflater layoutInflater = (LayoutInflater) LearnShareListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popup_window, null);
        popupWindow = new PopupWindow(customView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        if (Build.VERSION.SDK_INT >= 21) {
            popupWindow.setElevation(5.0f);
        }


        SharedPreferences preferences = getSharedPreferences("learnshare_pref", MODE_PRIVATE);
        String wish = preferences.getString("learnshare", "");

        SharedPreferences preferences2 = getSharedPreferences("discipline_pref", MODE_PRIVATE);
        String disc = preferences2.getString("discipline", "");

        SharedPreferences preferences3 = getSharedPreferences("subject_pref", MODE_PRIVATE);
        String sub = preferences3.getString("subject", "");


        if (disc.equals("")) {

        } else {

            helper.insertTableWish(disc, sub, wish);

            SharedPreferences preferencesls = getSharedPreferences("learnshare_pref", MODE_PRIVATE);
            SharedPreferences.Editor editorls = preferencesls.edit();
            editorls.putString("learnshare", "");
            editorls.apply();

            SharedPreferences preferencesd = getSharedPreferences("discipline_pref", MODE_PRIVATE);
            SharedPreferences.Editor editord = preferencesd.edit();
            editord.putString("discipline", "");
            editord.apply();

            SharedPreferences preferencess = getSharedPreferences("subject_pref", MODE_PRIVATE);
            SharedPreferences.Editor editors = preferencess.edit();
            editors.putString("subject", "");
            editors.apply();

        }


        if (listBeans.isEmpty()) {

        } else {

            listBeans.clear();

        }

        listBeans = helper.getWishes();
        lorSListAdapter = new LorSListAdapter(listBeans);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(lorSListAdapter);
        lorSListAdapter.notifyDataSetChanged();


        builder = new AlertDialog.Builder(LearnShareListActivity.this);
        builder.setMessage("Do you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        sessionManager.logoutUser();

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });


        logoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alert = builder.create();
                alert.setTitle("Logout");
                alert.show();

            }
        });


        addImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflater = (LayoutInflater) LearnShareListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.popup_window, null);
                popupWindow = new PopupWindow(customView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                if (Build.VERSION.SDK_INT >= 21) {
                    popupWindow.setElevation(5.0f);
                }

                discGrid = customView.findViewById(R.id.recycler_view);
                subGrid = customView.findViewById(R.id.recycler_view2);
                toggleSwitch = customView.findViewById(R.id.switchTog);
                sBtn = customView.findViewById(R.id.subMit);
                cBtn = customView.findViewById(R.id.canCel);


                disc_url = getString(R.string.discurl);
                final ConnectivityManager manager =
                        (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

                if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                        manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                        manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                        manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

                    new GetDiscipline().execute();

                } else {

                    Toast.makeText(getApplicationContext(), "No Internet, please try again!", Toast.LENGTH_SHORT).show();
                    finish();

                }


                final ArrayList<String> labels2 = new ArrayList<>();
                labels2.add("iLearn");
                labels2.add("iShare");
                toggleSwitch.setLabels(labels2);


                toggleSwitch.setOnToggleSwitchChangeListener(new BaseToggleSwitch.OnToggleSwitchChangeListener() {
                    @Override
                    public void onToggleSwitchChangeListener(int position, boolean isChecked) {

                        String wish = labels2.get(position).toString();
                        SharedPreferences preferences = getSharedPreferences("learnshare_pref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("learnshare", wish);
                        editor.apply();

                    }
                });


                adapter = new DisciplineAdapter(disciplineBeans);
                adapter.setOnItemClickListener(new DisciplineAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {

                        SharedPreferences preferences = getSharedPreferences("discipline_pref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("discipline", disciplineBeans.get(position).getDisc());
                        editor.putString("discid", disciplineBeans.get(position).getDid());
                        editor.apply();

                        sub_url = getString(R.string.suburl);
                        discid = disciplineBeans.get(position).getDid();

                        final ConnectivityManager manager =
                                (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
                        if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                                manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                                manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                                manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

                            new GetSubject().execute();

                        } else {

                            Toast.makeText(getApplicationContext(), "No Internet, please try again!", Toast.LENGTH_SHORT).show();
                            finish();

                        }

                    }
                });


                adapter2 = new SubjectAdapter(subjectBeans);
                adapter2.setOnItemClickListener(new SubjectAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {


                    }
                });


                sBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });


                cBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        popupWindow.dismiss();

                    }
                });


                popupWindow.showAtLocation(linearLayout1, Gravity.CENTER, 0, 0);

            }
        });


        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }


    @Override
    public void onBackPressed() {

        if (popupWindow.isShowing()) {

            popupWindow.dismiss();

        } else {

            finish();

        }

    }


    public class GetDiscipline extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                disc_resp = serviceHandler.makeServiceCall(disc_url, ServiceHandler.POST);

            } catch (Exception e) {

                e.printStackTrace();
                disc_resp = "Fail";

            }

            JSONObject jobject = null;

            try {

                jobject = new JSONObject(disc_resp);

                if (!disciplineBeans.isEmpty())
                    disciplineBeans.clear();

                for (int i = 0; i < jobject.getJSONArray("details").length(); i++) {

                    JSONObject jsonObject = jobject.getJSONArray("details").getJSONObject(i);
                    DisciplineBean bean = new DisciplineBean();
                    discid = jsonObject.getString("catid");
                    disc = jsonObject.getString("category");
                    discimgurl = jsonObject.getString("catimgurl");
                    bean.setDid(discid);
                    bean.setDisc(disc);
                    bean.setImagurl(discimgurl);
                    disciplineBeans.add(bean);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            new GetSubject().execute();

            discGrid.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                    LinearLayoutManager.HORIZONTAL, true));
            adapter = new DisciplineAdapter(disciplineBeans);
            discGrid.setAdapter(adapter);
            super.onPostExecute(aVoid);

        }

    }


    public class GetSubject extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> valuePairs = new ArrayList<>(1);

            if (discid.equals("") || discid.length() == 0) {

                valuePairs.add(new BasicNameValuePair("catid", "2"));

            } else {

                valuePairs.add(new BasicNameValuePair("catid", discid));

            }


            try {

                sub_resp = serviceHandler.makeServiceCall(sub_url, serviceHandler.POST, valuePairs);

            } catch (Exception e) {
                e.printStackTrace();
                sub_resp = "Fail";
            }

            JSONObject jsonObject = null;
            try {

                jsonObject = new JSONObject(sub_resp);

                if (!subjectBeans.isEmpty())
                    subjectBeans.clear();

                for (int i = 0; i < jsonObject.getJSONArray("details").length(); i++) {

                    JSONObject jsonObject1 = jsonObject.getJSONArray("details").getJSONObject(i);
                    SubjectBean bean = new SubjectBean();
                    subid = jsonObject1.getString("partid");
                    sub = jsonObject1.getString("particular");
                    subimgurl = jsonObject1.getString("partimgurl");
                    bean.setSid(subid);
                    bean.setSub(sub);
                    bean.setImagurl(subimgurl);
                    subjectBeans.add(bean);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            subGrid.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                    LinearLayoutManager.HORIZONTAL, true));
            adapter2 = new SubjectAdapter(subjectBeans);
            subGrid.setAdapter(adapter2);
            super.onPostExecute(aVoid);

        }

    }


}
