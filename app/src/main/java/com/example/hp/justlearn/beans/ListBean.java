package com.example.hp.justlearn.beans;

public class ListBean {

    String num, category, particulars, wish;

    public ListBean(){

    }

    public ListBean(String category, String particulars, String wish) {
        this.category = category;
        this.particulars = particulars;
        this.wish = wish;
    }

    public ListBean(String num, String category, String particulars, String wish) {
        this.num = num;
        this.category = category;
        this.particulars = particulars;
        this.wish = wish;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getParticulars() {
        return particulars;
    }

    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

}
